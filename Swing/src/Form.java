import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.EventQueue;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Form extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JButton plus;
	protected JButton exit;
	protected JLabel labelA;
	protected JLabel labelB;
	protected JLabel labelResult;
	protected JTextField fieldA;
	protected JTextField fieldB;
	protected JTextField result;
	public Form() {
		plus = new JButton("Plus");
		exit = new JButton("Exit");
		labelA = new JLabel("A");
		labelB = new JLabel("B");
		labelResult = new JLabel("Result");
		result = new JTextField();
		fieldA = new JTextField();
		fieldB = new JTextField();
		init();
	}
	private void init() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new GridLayout(4, 2));
		add(labelA);
		add(fieldA);
		add(labelB);
		add(fieldB);
		add(labelResult);
		add(result);
		add(plus);
		add(exit);
		
		this.repaint();
		result.setEnabled(false);
		plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) throws NumberFormatException{
				try {
					double a = Double.parseDouble(fieldA.getText());
					double b = Double.parseDouble(fieldB.getText());
					double c = a + b;
					String r = "" + c;
					result.setText(r);
				} catch (NumberFormatException ee) {
					System.out.println(ee);
					JOptionPane.showMessageDialog(null, ee);
				}
			}
		});
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		pack();
		setVisible(true);
		
	}
	public static void constructGUI() {
		new Form();
		//form.setVisible(true);
	}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				constructGUI();
			}
		});
	}
}
